# Add timestamp in Dolphin file manager

![](https://cn.pling.com/cache/167x167/img/8/1/2/b/cb77bbc11e34b4f48e982d6bb2b838738380.png)

Add-On for the Dolphin file manager in KDE 5, listed in the [KDE Store](https://store.kde.org/p/1187645/).

Announcement blog post [available here](https://bitleaf.de/2017/08/19/new-tool-add-timestamps-enhances-everyday-productivity/).

### Screenshot

![](media/screenshots/2017-08-19-add-timestamp-v0-2.png)

### Features

  - Can add two different timestamps to files and folders:
    - the current day (e.g. 2017-01-01) as a timestamp to files and folders.
    - the day on which the file was modified last.
  - The 'last modified' timestamp of a file or folder is not changed when adding the timestamp.

### Install
- Git clone this repo or download it.
- Run `sudo make install` inside the cloned resp. downloaded folder

### Uninstall
- Go inside this cloned/downloaded git repo
- Run `sudo make uninstall` cloned resp. downloaded folder in which the `Makefile` resides.

### Customize date format
You can change the the date format and the character between the date and the file name, e.g. to underscore instead of the default dash. Just edit line #11 in the *.desktop* files accordingly.

For instance, if you prefer the format `Y.m.d.`, like `2018.09.16. Testfile`, you need:
```
Exec=mv "%f" "$(pwd)/$(date '+%%Y.%%m.%%d. ')$(basename %f)"
```

Note that the double percent (`%%`) is needed in front of EVERY format sign (kudos for this note @klawdhfzasjhaa in [issue #5](https://github.com/bitleaf/kde5-dolphin-service-menu-add-timestamp/issues/5#)).


## Support this project

If you want this project to get better, support me with a few cents:

<a href="https://liberapay.com/MathiasRenner/"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>


### License

![](https://www.gnu.org/graphics/gplv3-127x51.png)

The project is licensed unter the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

Copyright (C) 2017 Mathias Renner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
