DIR=/usr/share/kservices5/ServiceMenus

install:
	mkdir -p $(DIR)

	cp add-timestamp-file-last-modified.desktop $(DIR)
	cp add-timestamp-today.desktop $(DIR)

	update-mime-database /usr/share/mime > /dev/null
	xdg-icon-resource forceupdate --theme hicolor
	xdg-desktop-menu forceupdate


uninstall:
	rm -f $(DIR)/add-timestamp-file-last-modified.desktop
	rm -f $(DIR)/add-timestamp-today.desktop

	update-mime-database /usr/share/mime > /dev/null
	xdg-icon-resource forceupdate --theme hicolor
	xdg-desktop-menu forceupdate
